<?php

namespace App\Http\Requests;

use App\Extensions\Request\APIRequest;

class ProductRequest extends APIRequest
{

    /**
     * Determine if we should return the first or all validation messages
     *
     * @var bool
     */
    protected $firstError = true;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $verb = $this->method();

        if ($verb === 'POST') {
            return [
                'name' => 'bail|required|string|min:3',
                'quantity' => 'bail|required|numeric|min:1',
                'price' => 'bail|required|numeric|min:0.01'
            ];
        } elseif ($verb === 'PUT') {
            return [
                'name' => 'bail|required|string|min:3',
                'quantity' => 'bail|required|numeric|min:1',
                'price' => 'bail|required|numeric|min:0.01',
                'date_created' => 'bail|required|date|date_format:Y-m-d H:i:s'
            ];
        } else {
            //
        }
    }
}
