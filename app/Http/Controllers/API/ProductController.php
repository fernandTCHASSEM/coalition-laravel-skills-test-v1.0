<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ProductRepository;
use App\Http\Requests\ProductRequest;

class ProductController extends Controller
{

    private $repository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->repository = $productRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->repository->paginate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $product = $this->repository->store($request->all());

        return response()->json([
            'code' => 201,
            'id' => $product->id
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = $this->repository->retrieve($id);

        if ($product !== false) {
            return response()->json([
                'code' => 200,
                'item' => $product
            ]);
        }

        return response()->json([
            'code' => 4002,
            'description' => 'This product doesn\'t exist.'
        ], 400);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, $id)
    {
        $inputs = $request->validated();
        $product = $this->repository->modify($inputs, $id);

        return response()->json([
            'code' => 200
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $isDelete = $this->repository->remove($id);

        if ($isDelete) {
            return response()->json([
                'code' => 200
            ], 200);
        }

        return response()->json([
            'code' => 4003,
            'description' => 'This product no longer exists.'
        ], 400);
    }
}
