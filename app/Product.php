<?php

namespace App;

use App\Extensions\Model\Model;

class Product extends Model
{

    public function __construct()
    {
        parent::__construct(database_path('tables/products.json'), [
            'id' => null, 'name' => null, 'quantity' => 0, 'price' => 0, 'date_created' => null
        ]);
    }

}
