<?php

namespace App\Repositories;

use App\Product;

class ProductRepository
{    

    function __construct(Product $product)
    {
        $this->model = $product;
    }

    public function store($arg)
    {
        $arg['date_created'] = date('Y-m-d H:i:s');
        return (new Product())->create($arg);
    }

    public function modify($arg, $id)
    {

        return (new Product())->update($id, $arg);
    }

    public function paginate()
    {
        return (new Product())->paginate();
    }

    public function remove($id)
    {
        return (new Product())->delete($id);
    }

    public function retrieve($id)
    {
        return (new Product())->find($id);
    }

}
