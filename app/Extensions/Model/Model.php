<?php

namespace App\Extensions\Model;

use ArrayAccess;
use JsonSerializable;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Pagination\Paginator;
use \Illuminate\Database\Eloquent\JsonEncodingException;
use Illuminate\Support\Collection;
use phpDocumentor\Reflection\Types\Integer;

class Model implements Jsonable, ArrayAccess, JsonSerializable
{

    /**
     * The model's attributes.
     *
     * @var array
     */
    protected $attributes = [];

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * The json file associated with the model.
     *
     * @var string
     */
    protected $table;

    /**
     * Create a new Eloquent model instance.
     *
     * @param string $table
     * @param  array  $attributes
     * @return void
     */
    public function __construct($table, array $attributes = [])
    {
        $this->table = $table;
        $this->attributes = $attributes;
    }

    /**
     * Fill the model with an array of attributes.
     *
     * @param  array  $attributes
     * @return $this
     */
    public function fill(array $attributes)
    {
        foreach ($attributes as $key => $value) {
            $this->setAttribute($key, $value);
        }

        return $this;
    }

    /**
     * Set a given attribute on the model.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return mixed
     */
    public function setAttribute($key, $value)
    {
        if (array_key_exists($key, $this->attributes)) {
            $this->attributes[$key] = $value;
        }

        return $this;
    }

    /**
     * Get an attribute from the model.
     *
     * @param  string  $key
     * @return mixed
     */
    public function getAttribute($key)
    {
        if (!$key) {
            return;
        }

        // If the attribute exists in the attribute array we will
        // get the attribute's value. This covers both types of values.
        if (array_key_exists($key, $this->attributes)) {
            return $this->attributes[$key];
        }

        // Here we will determine if the model base class itself contains this given key
        // since we don't want to treat any of those methods as relationships because
        // they are all intended as helper methods and none of these are relations.
        if (method_exists(self::class, $key)) {
            return;
        }
    }

    /**
     * Dynamically retrieve attributes on the model.
     *
     * @param  string  $key
     * @return mixed
     */
    public function __get($key)
    {
        return $this->getAttribute($key);
    }

    /**
     * Dynamically set attributes on the model.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return void
     */
    public function __set($key, $value)
    {
        $this->setAttribute($key, $value);
    }

    /**
     * Determine if the given attribute exists.
     *
     * @param  mixed  $offset
     * @return bool
     */
    public function offsetExists($offset)
    {
        return !is_null($this->getAttribute($offset));
    }

    /**
     * Get the value for a given offset.
     *
     * @param  mixed  $offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return $this->getAttribute($offset);
    }

    /**
     * Set the value for a given offset.
     *
     * @param  mixed  $offset
     * @param  mixed  $value
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        $this->setAttribute($offset, $value);
    }

    /**
     * Unset the value for a given offset.
     *
     * @param  mixed  $offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->attributes[$offset], $this->relations[$offset]);
    }

    /**
     * Determine if an attribute or relation exists on the model.
     *
     * @param  string  $key
     * @return bool
     */
    public function __isset($key)
    {
        return $this->offsetExists($key);
    }

    /**
     * Unset an attribute on the model.
     *
     * @param  string  $key
     * @return void
     */
    public function __unset($key)
    {
        $this->offsetUnset($key);
    }

    /**
     * Convert the model instance to JSON.
     *
     * @param  int  $options
     * @return string
     *
     * @throws \Illuminate\Database\Eloquent\JsonEncodingException
     */
    public function toJson($options = 0)
    {
        $json = json_encode($this->jsonSerialize(), $options);

        if (JSON_ERROR_NONE !== json_last_error()) {
            throw JsonEncodingException::forModel($this, json_last_error_msg());
        }

        return $json;
    }

    /**
     * Convert the object into something JSON serializable.
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return $this->toArray();
    }

    /**
     * Convert the model instance to an array.
     *
     * @return array
     */
    public function toArray()
    {
        return $this->attributes;
    }

    /**
     * Convert the model to its string representation.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->toJson();
    }

    /**
     * Get all of the items in the json file.
     *
     * @return array
     */
    public function all()
    {
        $jsonString = file_get_contents($this->table);

        $data = json_decode($jsonString);

        $data->items = array_reverse($data->items);

        return $data;
    }

    /**
     * find a single record by ID.
     *
     * @param  int    $id
     * @return mixed|static
     */
    public function find(int $id)
    {
        $data = $this->all();

        $items = $data->items;

        $find = array_filter($items, function ($item) use ($id) {
            return $item->id === $id;
        });

        if (!empty($find)) {
            return $find[0];
        } else {
            return false;
        }
    }

    public function paginate($perPage = 5)
    {
        $data = $this->all();

        $items = $data->items;

        $page = Paginator::resolveCurrentPage();

        return new \Illuminate\Pagination\Paginator(
            array_slice($items, ($page - 1) * $perPage),
            $perPage,
            $page
        );
    }

    /**
     * Save a new model and return the instance.
     *
     * @param  array  $attributes
     * @return \Extensions\Model|$this
     */
    public function create(array $attributes = [])
    {
        $attributes['id'] = $this->getLastId();
        $instance = $this->fill($attributes);

        $item = $instance->toArray();

        if ($this->store('ADD', $item)) {
            return $this;
        } else {
            return false;
        }
    }

    /**
     * Get the last id of items.
     *
     * @return int
     */
    public function getLastId()
    {
        $jsonString = file_get_contents($this->table);

        $data = json_decode($jsonString, true);

        $items = $data['items'];

        $lengthItems = count($items);

        if ($lengthItems !== 0) {
            $lastItem = $items[$lengthItems - 1]['id'];
            return $lastItem + 1;
        } else {
            return 1;
        }
    }

    /**
     * Update a model and return the instance.
     *
     * @param  int  $id
     * @param  array  $values
     * @return bool
     */
    public function update(int $id, array $values)
    {

        $this->id = $id;

        $jsonString = file_get_contents($this->table);

        $data = json_decode($jsonString, true);

        $updatedItems = array_map(function ($item) use ($id, $values) {
            if ($item['id'] === $id) {
                $this->fill($values);

                $item = $this->toArray();
            }

            return $item;
        }, $data['items']);

        $data['items'] = $updatedItems;

        if (!empty($this->attributes)) {
            if ($this->store('ALL', $data)) {
                return $this;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Delete a model and return a boolean.
     *
     * @param  int  $id
     * @return bool
     */
    public function delete(int $id)
    {
        $jsonString = file_get_contents($this->table);

        $data = json_decode($jsonString, true);

        $items = $data['items'];

        $key = null;

        while ($item = current($items)) {
            if ($item['id'] === $id) {
                $key = key($items);
                break;
            }
            next($items);
        }

        if ($key !== null) {
            unset($items[$key]);
            $data['items'] = $items;
            if ($this->store('ALL', $data)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Save a new item into the json file.
     *
     * @param  string  $operation
     * @param  array  $values
     * @return bool
     */
    private function store($operation = 'ALL', array $values = [])
    {
        if ($operation === 'ADD') {
            $jsonString = file_get_contents($this->table);

            $data = json_decode($jsonString, true);

            $data['items'][] = $values;

            $newJsonString = json_encode($data, JSON_PRETTY_PRINT);
        } else {
            $newJsonString = json_encode($values, JSON_PRETTY_PRINT);
        }

        if (file_put_contents($this->table, stripslashes($newJsonString)) !== false) {
            return true;
        } else {
            return false;
        }
    }

}
