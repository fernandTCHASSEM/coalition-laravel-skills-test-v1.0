<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- IMPORT FONT FAMILY -->
    <link href="https://fonts.googleapis.com/css?family=Alex+Brush" rel="stylesheet">

    <!-- BOOTSTRAP GLYPHICON -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

    <!-- XCSRF PROTECTION -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- API URL -->
    <meta name="url-api" content="{{ url('/api') }}">

    <!-- Bootstrap CSS -->
    {!! Html::style('plugins/bootstrap-4.2.1/css/bootstrap.min.css') !!}

    <!-- Customized CSS -->
    {!! Html::style('css/customized.css') !!}

    <title>List of products</title>
</head>

<body>
    <nav class="navbar navbar-default">
        <a class="navbar-brand" href="{{ url('home') }}">
            {{-- <div class="img-logo">
                <img class="img-responsive" src="{{URL::asset('/img/logo2.png')}}" alt="">
            </div> --}}
            <img src="{{URL::asset('/img/logo2.png')}}" width="30" height="60" class="d-inline-block align-middle ml-4"
                alt="">
            <h3 class="d-inline-block align-middle mr-1">Book</h3>
        </a>
    </nav>
    <div class="container-fluid mt-2">
        <!-- The product's id to edit or delete -->
        <input type="hidden" id="idProduct"/>

        <!-- Modal for create a product -->
        <div class="row d-flex justify-content-center">
            <div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="modalAdd" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header text-center">
                            <h4 class="modal-title w-100 font-weight-bold text-primary ml-5">Add new product</h4>
                            <button type="button" class="close text-primary" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body mx-3">
                            <form id="formAdd" novalidate>
                                <div class="md-form mb-5">
                                    <input type="text" id="inputNameAdd" required="required" minlength="3" class="form-control validate">
                                    <label for="inputName">Name</label>
                                </div>

                                <div class="md-form mb-5">
                                    <input type="number" id="inputQuantityAdd" min="1" required="required" class="form-control validate">
                                    <label for="inputQuantity">Quantity</label>
                                </div>

                                <div class="md-form mb-5">
                                    <input type="number" id="inputPriceAdd" min="0.01" required="required" class="form-control validate">
                                    <label for="inputPrice">Price</label>
                                </div>

                                <div class="alert alert-danger m-0 d-none" id="errorAdd" role="alert"></div>
                            </form>
                        </div>
                        <div class="modal-footer d-flex justify-content-center">
                            <button class="btn btn-outline-primary btn-block" id="btnAdd">Add product
                                <i class="fas fa-paper-plane-o ml-1"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal for edit a product -->
            <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="modalEdit" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header text-center">
                            <h4 class="modal-title w-100 font-weight-bold text-primary ml-5">Edit a product</h4>
                            <button type="button" class="close text-primary" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body mx-3">
                            <form id="formEdit" novalidate>
                                <input type="hidden" id="creationDate"/>
                                <div class="md-form mb-5">
                                    <input type="text" id="inputNameEdit" required="required" minlength="3" class="form-control validate">
                                    <label for="inputName" class="active">Name</label>
                                </div>

                                <div class="md-form mb-5">
                                    <input type="number" min="1" id="inputQuantityEdit" required="required" class="form-control validate">
                                    <label for="inputQuantity" class="active">Quantity</label>
                                </div>

                                <div class="md-form mb-5">
                                    <input type="number" min="0.1" id="inputPriceEdit" required="required" class="form-control validate">
                                    <label for="inputPrice" class="active">Price</label>
                                </div>

                                <div class="alert alert-danger m-0 d-none" id="errorEdit" role="alert"></div>
                            </form>
                        </div>
                        <div class="modal-footer d-flex justify-content-center">
                            <button class="btn btn-outline-info btn-block"  id="btnEdit">Edit product
                                <i class="fas fa-paper-plane-o ml-1"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal for delete product -->
            <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="modalDelete"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header text-center">
                            <h4 class="modal-title w-100 font-weight-bold ml-5 text-danger">Delete</h4>
                            <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body mx-3">
                            <p class="text-center h4">Are you sure to delete selected product ?</p>
                        </div>
                        <div class="modal-footer d-flex justify-content-center deleteButtonsWrapper">
                            <button type="button" class="btn btn-danger btnYesClass" id="btnYes">Yes</button>
                            <button type="button" class="btn btn-primary btnNoClass" id="btnNo" data-dismiss="modal">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mb-2 float-right">
            <div class="col d-inline">
                <a href="" class="btn btn-primary button-rounded text-center" data-toggle="modal" data-target="#modalAdd"><i class="fas fa-plus"></i></a>
            </div>
            <div class="col d-inline">
                <a href="" class="btn btn-danger button-rounded text-center disabled" id="showModalEdit"><i class="fas fa-pen"></i></a>
            </div>
            <div class="col d-inline">
                <a href="" class="btn btn-info button-rounded text-center disabled" id="showModalDelete"><i class="fas fa-minus"></i></a>
            </div>
        </div>
        <table id="tableProducts" class="table table-striped table-bordered m" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th class="th-sm">Name</th>
                    <th class="th-sm">Quantity</th>
                    <th class="th-sm">Price</th>
                    <th class="th-sm">Creation date</th>
                    <th scope="col">Total</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
                <tr>
                    <th scope="col">#</th>
                    <th class="th-sm">Name</th>
                    <th class="th-sm">Quantity</th>
                    <th class="th-sm">Price</th>
                    <th class="th-sm">Creation date</th>
                    <th scope="col">Total</th>
                </tr>
            </tfoot>
        </table>
        <nav aria-label="Page navigation" class="float-right">
            <ul class="pagination">
                <li class="page-item"><a class="page-link" href="#" id="previousPage"><i class="fas fa-chevron-left"></i></a></li>
                <li class="page-item"><a class="page-link" href="#"  id="nextPage"><i class="fas fa-chevron-right"></i></a></li>
            </ul>
        </nav>
        <template id="productrow">
            <tr>
                <td scope="row">1</th>
                <td>fake</td>
                <td>fake</td>
                <td class="text-right">fake</td>
                <td>fake</td>
                <td class="text-right">fake</td>
            </tr>
        </template>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    {!! Html::script('plugins/jquery/jquery-3.3.1.min.js') !!}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
        crossorigin="anonymous"></script>
    {!! Html::script('plugins/bootstrap-4.2.1/js/bootstrap.min.js') !!}
    {!! Html::script('js/main.js') !!}
</body>

</html>
