window.addEventListener("load", function () {
    let url = document.querySelector("meta[name='url-api']").getAttribute("content"),
        errorAdd = document.querySelector("#errorAdd"),
        errorEdit = document.querySelector("#errorEdit"),
        formAdd = document.querySelector("#formAdd"),
        inputNameAdd = document.querySelector("#inputNameAdd"),
        inputQuantityAdd = document.querySelector("#inputQuantityAdd"),
        inputPriceAdd = document.querySelector("#inputPriceAdd"),
        formEdit = document.querySelector("#formEdit"),
        idProduct = document.querySelector("#idProduct"),
        creationDate = document.querySelector("#creationDate"),
        inputNameEdit = document.querySelector("#inputNameEdit"),
        inputQuantityEdit = document.querySelector("#inputQuantityEdit"),
        inputPriceEdit = document.querySelector("#inputPriceEdit"),
        t = document.querySelector('#productrow'),
        tb = document.querySelector("tbody"),
        previousNavigation = document.querySelector("#previousPage"),
        nextNavigation = document.querySelector("#nextPage"),
        refreshTable = async (suffixUrl) => {
                tb.innerHTML = "<tr class=\"no-reaction\"><td colspan=\"5\">Loading...</td></tr>";
                await fetch(url + suffixUrl)
                    .then(response => response.json())
                    .then(json => {
                        let items = json.data,
                            clone = null,
                            td = null,
                            quantity = 0,
                            price = 0;

                        tb.innerHTML = "";

                        for (let index = 0, nbrItems = items.length; index < nbrItems; index++) {
                            clone = document.importNode(t.content, true);

                            clone.querySelector("tr").dataset.json = JSON.stringify(items[index]);

                            td = clone.querySelectorAll("td");

                            quantity = items[index].quantity;
                            price = items[index].price;

                            td[0].textContent = items[index].id;
                            td[1].textContent = items[index].name;
                            td[2].textContent = quantity;
                            td[3].textContent = "$" + price;
                            td[4].textContent = items[index].date_created;
                            td[5].textContent = "$" + (price * quantity);

                            tb.appendChild(clone);
                        }

                        refreshPagination(json.prev_page_url, json.next_page_url);
                    })
                    .catch(error => console.error('Error:', error))
            },
            refreshPagination = (previousPageUrl = null, nextPageUrl = null) => {
                if (previousPageUrl !== null) {
                    previousNavigation.parentNode.classList.remove("disabled");
                    previousNavigation.setAttribute("href", previousPageUrl);
                } else {
                    previousNavigation.parentNode.classList.add("disabled");
                }

                if (nextPageUrl !== null) {
                    nextNavigation.parentNode.classList.remove("disabled");
                    nextNavigation.setAttribute("href", nextPageUrl);
                } else {
                    nextNavigation.parentNode.classList.add("disabled");
                }
            },
            validateRequired = (input) => {
                if (input.validity.valueMissing) {
                    return "The " + ((input.nextElementSibling.textContent).trim()).toLowerCase() + " field is required."
                }

                return false;
            },
            validateLength = (input) => {
                if (input.validity.tooShort) {
                    return "The " + ((input.nextElementSibling.textContent).trim()).toLowerCase() + " must be at least " + input.getAttribute("minlength") + " characters."
                }

                return false;
            },
            validateFormAdd = () => {
                let errorMsg = null;

                if (errorMsg = validateRequired(inputNameAdd)) {
                    return errorMsg;
                }

                if (errorMsg = validateLength(inputNameAdd)) {
                    return errorMsg;
                }

                if (errorMsg = validateRequired(inputPriceAdd)) {
                    return errorMsg;
                }

                if (errorMsg = validateRequired(inputQuantityAdd)) {
                    return errorMsg;
                }

                return false;
            },
            validateFormEdit = () => {
                let errorMsg = null;

                if (errorMsg = validateRequired(inputNameEdit)) {
                    return errorMsg;
                }

                if (errorMsg = validateLength(inputNameEdit)) {
                    return errorMsg;
                }

                if (errorMsg = validateRequired(inputPriceEdit)) {
                    return errorMsg;
                }

                if (errorMsg = validateRequired(inputQuantityEdit)) {
                    return errorMsg;
                }

                return false;
            },
            resetFormEdit = () => {
                formEdit.reset();
            },
            resetFormAdd = () => {
                formAdd.reset();
                document.querySelectorAll("#formAdd .md-form label.active").forEach(function (element) {
                    element.classList.remove("active");
                });
            };

    previousNavigation.addEventListener("click", function (event) {
        event.preventDefault();
        refreshTable("/product" + event.currentTarget.getAttribute("href"));
    }, false);

    nextNavigation.addEventListener("click", function (event) {
        event.preventDefault();
        refreshTable("/product" + event.currentTarget.getAttribute("href"));
    }, false);

    refreshTable("/product");

    document.querySelector("#btnAdd").addEventListener("click", function () {

        let isInvalidated = validateFormAdd();

        if (isInvalidated === false) {

            errorAdd.classList.add("d-none");
            this.setAttribute("disabled", true);

            fetch(url + "/product", {
                    method: 'POST',
                    body: JSON.stringify({
                        "name": inputNameAdd.value,
                        "quantity": inputQuantityAdd.value,
                        "price": inputPriceAdd.value
                    }),
                    headers: {
                        "Content-Type": "application/json"
                    }
                }).then(res => res.json())
                .then(async json => {
                    if (json.code === 201) {
                        resetFormAdd();
                        await refreshTable("/product");
                        $("#modalAdd").modal("hide");
                    } else if (json.code === 4000) {
                        let concerned = Object.keys(json.errors)[0],
                            errors = json.errors[concerned];
                        errorAdd.innerHTML = errors[0];
                        errorAdd.classList.remove("d-none");
                    }
                    this.removeAttribute("disabled");
                })
                .catch(error => {
                    this.removeAttribute("disabled");
                    console.error('Error:', error);
                });
        } else {
            errorAdd.innerHTML = isInvalidated;
            errorAdd.classList.remove("d-none");
        }
    }, false);

    document.querySelectorAll(".md-form input").forEach(function (element) {
        element.addEventListener("focus", function () {
            this.nextElementSibling.classList.add("active");
        }, false);

        element.addEventListener("blur", function () {
            if (!this.value) {
                this.nextElementSibling.classList.remove("active");
            }
        }, false);
    });

    tb.addEventListener("click", function (event) {
        let element = event.target;
        if (element !== event.currentTarget) {
            if (element.tagName === "TD") {
                if (element.parentNode.classList.contains("tr-color-selected")) {
                    element.parentNode.classList.remove("tr-color-selected");
                    document.querySelector("#showModalEdit").classList.add("disabled");
                    document.querySelector("#showModalDelete").classList.add("disabled");
                } else {
                    let rowAlreadySelected = document.querySelector("tbody .tr-color-selected");
                    if (rowAlreadySelected !== null) {
                        rowAlreadySelected.classList.remove("tr-color-selected");
                        document.querySelector("#showModalEdit").classList.add("disabled");
                        document.querySelector("#showModalDelete").classList.add("disabled");
                    }
                    element.parentNode.classList.add("tr-color-selected");
                    document.querySelector("#showModalEdit").classList.remove("disabled");
                    document.querySelector("#showModalDelete").classList.remove("disabled");
                }
            }

        }
    }, false);

    document.querySelector("#showModalEdit").addEventListener("click", function (event) {
        event.preventDefault();

        let data =  JSON.parse(document.querySelector("tbody .tr-color-selected").dataset.json);
        idProduct.value = data.id;
        creationDate.value = data.date_created;
        inputNameEdit.value = data.name;
        inputPriceEdit.value = data.price;
        inputQuantityEdit.value = data.quantity;
        $("#modalEdit").modal("show");
    }, false);

    document.querySelector("#btnEdit").addEventListener("click", function () {

        let isInvalidated = validateFormEdit();

        if (isInvalidated === false) {

            errorEdit.classList.add("d-none");
            this.setAttribute("disabled", true);

            fetch(url + "/product/" + idProduct.value, {
                    method: 'PUT',
                    body: JSON.stringify({
                        "name": inputNameEdit.value,
                        "quantity": inputQuantityEdit.value,
                        "price": inputPriceEdit.value,
                        "date_created": creationDate.value
                    }),
                    headers: {
                        "Content-Type": "application/json"
                    }
                }).then(res => res.json())
                .then(async json => {
                    if (json.code === 200) {
                        resetFormEdit();
                        await refreshTable("/product");
                        $("#modalEdit").modal("hide");
                    } else if (json.code === 4000) {
                        let concerned = Object.keys(json.errors)[0],
                            errors = json.errors[concerned];
                            errorEdit.innerHTML = errors[0];
                            errorEdit.classList.remove("d-none");
                    }
                    this.removeAttribute("disabled");
                    document.querySelector("#showModalEdit").classList.add("disabled");
                    document.querySelector("#showModalDelete").classList.add("disabled");
                })
                .catch(error => {
                    this.removeAttribute("disabled");
                    console.error('Error:', error);
                });
        } else {
            errorEdit.innerHTML = isInvalidated;
            errorEdit.classList.remove("d-none");
        }
    }, false);

    document.querySelector("#showModalDelete").addEventListener("click", function (event) {
        event.preventDefault();

        let data =  JSON.parse(document.querySelector("tbody .tr-color-selected").dataset.json);
        idProduct.value = data.id;
        $("#modalDelete").modal("show");
    }, false);

    document.querySelector("#btnYes").addEventListener("click", function () {
        this.setAttribute("disabled", true);

        fetch(url + "/product/" + idProduct.value, {
            method: 'DELETE',
            headers: {
                "Content-Type": "application/json"
            }
        }).then(res => res.json())
        .then(async json => {
            if (json.code === 200) {
                await refreshTable("/product");
                $("#modalDelete").modal("hide");
            }
            this.removeAttribute("disabled");
            document.querySelector("#showModalEdit").classList.add("disabled");
            document.querySelector("#showModalDelete").classList.add("disabled");
        })
        .catch(error => {
            this.removeAttribute("disabled");
            console.error('Error:', error);
        });
    }, false);
});
